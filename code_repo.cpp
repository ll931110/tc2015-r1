/*
	Implementation of SmallPolygons: Topcoder 2015 Marathon Match, Round 1
	Some lessons will be reserved for future rounds:
	- Delauney triangulation
	- Incremental growth of polygon
	- Various techniques of diving the plane
*/

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#define PI acos(-1.0)
using namespace std;

/*
	The Point class, representing a single point on the plane
*/
struct Point {
	int x, y, id;

	Point() {
	}

	Point(int _x, int _y, int _id) {
		x = _x;
		y = _y;
		id = _id;
	}

	int getID() {
		return id;
	}

	// Calculate the cross product of this point, p1 and p2 in that order
	int crossProduct(Point p1, Point p2) {
		int ax = x - p1.x;
		int ay = y - p1.y;
		int bx = p1.x - p2.x;
		int by = p1.y - p2.y;
		if (ax * by > ay * bx) {
			return 1;
		}
		else if (ax * by < ay * bx) {
			return -1;
		}
		return 0;
	}

	bool isXBetween(Point p1, Point p2) {
		return min(p1.x, p2.x) <= x && x <= max(p1.x, p2.x);
	}

	// Check if this point is lexicalgraphically smaller than the other point
	bool isLexicSmaller(Point other) {
		return (x < other.x || (x == other.x && y < other.y));
	}

	bool isLexicGreater(Point other) {
		return (x > other.x || (x == other.x && y > other.y));
	}

	double distanceTo(Point p) {
		return sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));
	}

	// Return the angle between this point and p. The angle will be in [0, 2 * Pi]
	double angleTo(Point p) {
		double angle = atan2(p.x - x, p.y - y);
		if (angle < 0) {
			angle += 2 * PI;
		}
		return angle;
	}

	void swap(Point& p) {
		using std::swap;
		swap(x, p.x);
		swap(y, p.y);
		swap(id, p.id);
	}
};

struct Line {
	Point p1, p2;

	Line(Point _p1, Point _p2) {
		// TODO: add deep copying
		p1 = Point(_p1.x, _p1.y, _p1.id);
		p2 = Point(_p2.x, _p2.y, _p2.id);
	}

	// Check if this line intersects another line. For now we assume lines don't have the same points.
	bool intersect(Line other) {
		int s1 = p1.crossProduct(other.p1, other.p2);
		int s2 = p2.crossProduct(other.p1, other.p2);
		int s3 = other.p1.crossProduct(p1, p2);
		int s4 = other.p2.crossProduct(p1, p2);

		if (s1 == 0) {
			if (p1.isXBetween(other.p1, other.p2)) {
				return true;
			}
		}
		if (s2 == 0) {
			if (p2.isXBetween(other.p1, other.p2)) {
				return true;
			}
		}
		if (s3 == 0) {
			if (other.p1.isXBetween(p1, p2)) {
				return true;
			}
		}
		if (s4 == 0) {
			if (other.p2.isXBetween(p1, p2)) {
				return true;
			}
		}
		if (s1 * s2 < 0 && s3 * s4 < 0) {
			return true;
		}
		return false;
	}
};

struct Polygon {
	vector<Point> index;

	Polygon() {
	}

	// Create a Polygon instance from a set of indices
	Polygon(vector<Point> _index) {
		index = _index;
	}

	// Get the previous point of the polygon
	int prev(int i) {
		return (i + index.size() - 1) % index.size();
	}

	// Get the next point of the polygon
	int next(int i) {
		return (i + 1) % index.size();
	}

	// Add a new point. It does NOT guarantee that we will get a simple polygon in this order
	// Polygonize will do that
	void add(Point p) {
		index.push_back(p);
	}

	// Sort the points in increasing order of X-coordinate
	void XSort() {
		// sort the points
		for (int i = 0; i + 1 < index.size(); i++) {
			for (int j = i + 1; j < index.size(); j++) {
				if (index[i].isLexicGreater(index[j])) {
					index[i].swap(index[j]);
				}
			}
		}
	}

	vector<Point> makeVector(int first, int last) {
		vector<Point> returnedVector;
		for (int i = first; i < last; i++) {
			returnedVector.push_back(index[i]);
		}
		return returnedVector;
	}

	// This is a simple algorithm for polygonize a set of points
	// Sort the points in the increasing order of x-axis, take the leftmost (p) and rightmost (q) point
	// Divide the set into the upper and lower. Connect p with upper part, then to q, then to lower part in reverse order
	Polygon polygonize() {
		XSort();
		// Take the first point and last point
		vector<Point> upper, lower;
		for (int i = 1; i + 1 < index.size(); i++) {
			if (index[i].crossProduct(index[0], index[index.size() - 1]) >= 0) {
				upper.push_back(index[i]);
			}
			else {
				lower.push_back(index[i]);
			}
		}
		vector<Point> answer;
		answer.push_back(index[0]);
		for (int i = 0; i < upper.size(); i++) {
			answer.push_back(upper[i]);
		}
		answer.push_back(index[index.size() - 1]);
		for (int i = lower.size() - 1; i >= 0; i--) {
			answer.push_back(lower[i]);
		}
		return Polygon(answer);
	}

	Polygon permuteReject() {
		if (index.size() < 3) {
			return Polygon(vector<Point>());
		}
		vector<int> perm(index.size());
		for (int i = 0; i < index.size(); i++) {
			swap(perm[i], perm[rand() % (i + 1)]);
		}
		vector<Point> ordering;
		
		// Boundary check: if i = 2, we don't need to check anything
		// if i = 3, then check 0-1 and 2-3, as well as 1-2 and 3-0
		for (int i = 0; i < perm.size(); i++) {
			ordering.push_back(index[perm[i]]);
			if (i > 0) {
				Line referenceLine = Line(ordering[i - 1], ordering[i]);
				for (int j = 0; j + 2 < i; j++) {
					Line previousLine = Line(ordering[j], ordering[j + 1]);
					if (referenceLine.intersect(previousLine)) {
						// invalid polygon
						return Polygon();
					}
				}
			}			
		}

		// check for the closing line
		Line referenceLine = Line(ordering[ordering.size() - 1], ordering[0]);
		for (int i = 1; i + 2 < ordering.size(); i++) {
			Line previousLine = Line(ordering[i], ordering[i + 1]);
			if (referenceLine.intersect(previousLine)) {
				return Polygon();
			}
		}
		return Polygon(ordering);
	}

	// Check if the polygon can be polygonizable. 
	// Requirement: it needs at least 3 points and not all points are on the same line
	bool isPolygonizable() {
		if (index.size() < 3) {
			return false;
		}
		for (int i = 0; i + 2 < index.size(); i++) {
			if (index[0].crossProduct(index[i + 1], index[i + 2]) != 0) {
				return true;
			}
		}
		return false;
	}

	// Return the optimal polygon choice
	Polygon optimize() {		
		if (!isPolygonizable()) {
			return Polygon();
		}
		Polygon naivePolygon = polygonize();
		Polygon* bestPolygon = &naivePolygon;
		double bestArea = bestPolygon->area();

		// Only check random permutation if the size is small
		if (naivePolygon.index.size() < 15) {
			for (int iter = 0; iter < 100; iter++) {
				Polygon samplePolygon = permuteReject();
				double tmpArea = samplePolygon.area();
				if (tmpArea > 0 && tmpArea < bestArea) {
					bestArea = tmpArea;
					bestPolygon = &samplePolygon;
				}
			}
		}

		return *bestPolygon;
	}

	// Produce the space-separated indices of points
	string serialize() {
		string answer = "";
		for (int i = 0; i < index.size(); i++) {
			if (i > 0) {
				answer += ' ';
			}
			int pointID = index[i].getID();
			ostringstream stringConvert;
			stringConvert << pointID;
			answer += stringConvert.str();
		}
		return answer;
	}

	// Calculate the polygon perimeter. TODO: Currently unused
	double perimeter() {
		double perm = 0;
		for (int i = 0; i < index.size(); i++) {
			perm += index[i].distanceTo(index[next(i)]);
		}
		return perm;
	}

	// Calculate the polygon area using http://en.wikipedia.org/wiki/Shoelace_formula
	double area() {
		if (!isPolygonizable()) {
			return -1.0;
		}
		double ans = 0;
		for (int i = 0; i < index.size(); i++) {
			ans += index[i].x * index[next(i)].y;
			ans -= index[i].y * index[next(i)].x;
		}
		return abs(ans)/2;
	}
};

struct SpacePartition {
	// Second algorithm: divide the plane into a few disjoint pieces, and construct a polygon on each piece.
	// For now we shall divide in rectangular fashion. The problem is that some polygons may be invalid.
	// How do we deal with that? For now, we will abort that division
	vector<Polygon> polygons;
	vector<Point> pts;
	int minX, maxX, minY, maxY;

	// TODO: add intersection checks between polygons

	SpacePartition(vector<Point> _pts) {
		pts = _pts;
		// calculate the boundary of the points
		minX = -1;
		maxX = -1;
		minY = -1;
		maxY = -1;
		for (int i = 0; i < pts.size(); i++) {
			if (minX < 0 || pts[i].x < minX) {
				minX = pts[i].x;
			}
			if (maxX < 0 || pts[i].x > maxX) {
				maxX = pts[i].x;
			}
			if (minY < 0 || pts[i].y < minY) {
				minY = pts[i].y;
			}
			if (maxY < 0 || pts[i].y > maxY) {
				maxY = pts[i].y;
			}
		}
	};

	Point randomCoordinate() {
 		int xCoord = rand() % (maxX - minX + 1) + minX;
 		int yCoord = rand() % (maxY - minY + 1) + minY;
 		return Point(xCoord, yCoord, -1); // we don't need the index here
 	}

	// Polygonize a set of polygons
	void polygonize() {
 		for (int i = 0; i < polygons.size(); i++) {
 			if (!polygons[i].isPolygonizable()) {
 				return;
 			}
 			polygons[i] = polygons[i].optimize();
 		} 		
 	}

	// Divide the plane into boxes of m * n, limited by the boundaries of the points
	void addRectScheme(int m, int n) {
		polygons.clear();
		for (int i = 0; i < m * n; i++) {
			polygons.push_back(Polygon());
		}
		
		double cellWidth = (double) (maxX - minX)/n;
		double cellHeight = (double) (maxY - minY)/m;
		for (int i = 0; i < pts.size(); i++) {
			int rowOffset = (int) floor((pts[i].y - minY)/cellHeight);
			if (rowOffset >= m) {
				rowOffset = m - 1;
			}
			int colOffset = (int) floor((pts[i].x - minX)/cellWidth);
			if (colOffset >= n) {
				colOffset = n - 1;
			}
			polygons[rowOffset * n + colOffset].add(pts[i]);
		}
		polygonize();
 	};


 	// Pick a center point and divide the plane radially. Add points with 2 pi/n sector
 	// If the center coincides with some point, count that to the first sector
 	void addRadialScheme(int numSector, double angleOffset) {
 		Point origin = randomCoordinate();
 		polygons.clear();
 		for (int i = 0; i < numSector; i++) {
 			polygons.push_back(Polygon()); 			
 		}
 		double sectorAngle = 2 * PI / numSector;
 		for (int i = 0; i < pts.size(); i++) {
 			double angle = origin.angleTo(pts[i]) - angleOffset;
 			if (angle < 0) {
 				angle += 2 * PI;
 			}
 			int sectorOffset = (int) floor(angle/sectorAngle);
 			polygons[sectorOffset].add(pts[i]);
 		}
 		polygonize();
 	}

 	// We resemble k means clustering, but without moving the origin
 	// Pick N random points and start N circles, each initially has radius 0.
 	// Each time, pick a point and add to some circles, expanding the circle if necessary
 	// Abort the process if circles intersect
 	void circleExpansion(int numPolygons) {
 		polygons.clear();
 		for (int i = 0; i < numPolygons; i++) {
 			polygons.push_back(Polygon()); 			
 		}

 		vector<Point> origin;
 		vector<double> radius;

 		for (int i = 0; i < numPolygons; i++) {
 			origin.push_back(randomCoordinate());
 			radius.push_back(0);
 		}

 		for (int i = 0; i < pts.size(); i++) {
 			bool needExpanding = true;
 			for (int j = 0; j < numPolygons; j++) {
 				if (pts[i].distanceTo(origin[j]) <= radius[j]) {				
 					polygons[j].add(pts[i]);
 					needExpanding = false;
 					break;
 				}
 			}
 			if (!needExpanding) {
 				continue;
 			}
 			// find the closet origin
 			int closetCircle = 0;
 			for (int j = 1; j < numPolygons; j++) {
 				if (pts[i].distanceTo(origin[j]) < pts[i].distanceTo(origin[closetCircle])) {
 					closetCircle = j;
 				}
 			}
 			// Expand the new circle and check if it is better
 			radius[closetCircle] = pts[i].distanceTo(origin[closetCircle]);
 			for (int j = 0; j < numPolygons; j++) {
 				if (j != closetCircle && origin[j].distanceTo(origin[closetCircle]) < radius[j] + radius[closetCircle]) {
 					return;
 				}
 			}
 			// add this point to closetCircle polygon 			
 			polygons[closetCircle].add(pts[i]);
 		}
 		polygonize();
 	}

 	// Return the total area of all polygons. If any of them is invalid, return -1
	double area() {
		double sumArea = 0;
		int ptsCount = 0;
		for (int i = 0; i < polygons.size(); i++) {
			ptsCount += polygons[i].index.size();
			if (!polygons[i].isPolygonizable()) {
				return -1.0;
			}
			sumArea += polygons[i].area();
		}
		if (ptsCount != pts.size()) {
			return -1.0;
		}
		return sumArea;
	}

	// Return the serialize (space-integer)
	vector<string> serialize() {
		vector<string> result;
		for (int i = 0; i < polygons.size(); i++) {
			result.push_back(polygons[i].serialize());
		}
		return result;
	}
};

struct SmallPolygons {
	
	vector<Point> myPoint;
	int numPolygons;
	SpacePartition scheme = SpacePartition(vector<Point>()); // TODO: we can't leave it undeclared

	double minArea;
	vector<string> minSetOfPolygons;

	vector<string> choosePolygons(vector<int> points, int N) {
		minArea = -1;
		int numPoints = points.size() / 2;
		for (int i = 0; i < numPoints; i++) {
			myPoint.push_back(Point(points[2 * i], points[2 * i + 1], i));
		}
		scheme = SpacePartition(myPoint);

		numPolygons = N;
		for (int m = 1; m <= N; m++) {
			for (int n = 1; m * n <= N; n++) {
				scheme.addRectScheme(m, n);
				updateResult();
			}
		}

		for (int iter = 0; iter < 1000; iter++) {
			double angleOffset = (double) rand() / RAND_MAX * 2 * PI;
			scheme.addRadialScheme(numPolygons, angleOffset);
			updateResult();
			scheme.circleExpansion(numPolygons);
			updateResult();
		}

		return minSetOfPolygons;
	};

	void updateResult() {
		double tmpArea = scheme.area();
		if (tmpArea >= 0 && (minArea < 0 || minArea > tmpArea)) {			
			minArea = tmpArea;
			minSetOfPolygons = scheme.serialize();
		}
	}
};
