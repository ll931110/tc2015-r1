#include <iostream>
#include "code_repo.cpp"
using namespace std;

int main() {
	int Np;
	cin >> Np;
	cerr << Np << endl;
	vector<int> points;
	for (int i = 0; i < Np; i++) {
		int coord;
		cin >> coord;
		// cerr << coord << ' ';
		points.push_back(coord);
	}
	// cerr << endl;
	int N;
	cin >> N;
	// cerr << N << endl;

	SmallPolygons object = SmallPolygons();
	vector<string> ans = object.choosePolygons(points, N);
	cout << ans.size() << endl;
	for (int i = 0; i < ans.size(); i++) {
		cout << ans[i] << endl;
		// cerr << ans[i] << endl;
	}
	cout << flush;
	return 0;
}
